
public class StringExample {

	public static void main(String[] args) {

		// TODO Auto-generated method stub

		
		//Create a string
		
		String name  = "Arya";
		System.out.println(name);
		
		//2.Number of character in the string
		int numChars = name.length();
		System.out.println("number of character"+ numChars);
		
		
		//3.get a specific character
		char CharacterAtposition = name.charAt(2);
		System.out.println("character position:" +CharacterAtposition);
		
		
		//4. Get a substring
		
		String sub = name.substring(0,2);
		System.out.println("substring:" +sub);
		
		String sub01 = name.substring(1);
		System.out.println("substring:" +sub01);
		
		
		//5. if one string is equal to another
		
	 String a = "Arya";
		String b = "Athira";
		String c = "emad";
		
		if(a.contentEquals(b)) {
			
			System.out.println("a & b are same");
			
		}
		else
			
		{
			System.out.println("a & c are not smae");
		}
		
		//6. make everything is uppercase
		
		String m = "yelling";
		System.out.println(m.toUpperCase());
		
		//5.make everything in lowercase
		
		String p = "YELLING";
		System.out.println(p.toLowerCase());
		
	}

}
